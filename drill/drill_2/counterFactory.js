function counterFactory() {
  
    let counter = 0;
  
    return {
      increment() {
        counter++;
        return counter;
      },
      decrement() {
        counter--;
        return counter;
      },
      x (){
        return counter;
      }
    }
}

const methods = counterFactory();

const inc = methods.increment;
const dec = methods.decrement;
console.log(inc());
console.log(inc());
console.log(dec());