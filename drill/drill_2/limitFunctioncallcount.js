
function limitFunctionCallCount(cb, n) {
    return () => {
        for (let i = 0; i < n; i++) {
          cb();
        }
      };
    }
    function cb() {
        console.log("hello harry");
      }
      
      limitFunctionCallCount(cb, 6)();