
const data = [{
    23021930923: {
      quantity: 2,
      price: "$3",
      name: "Pen"
    },
    23243243243: {
      quantity: 2,
      price: "$12",
      name: "Book"
    },
    092139029130: {
      quantity: 6,
      price: "$1",
      name: "Eraser"
    },
    839439829489: {
      quantity: 3,
      price: "$6",
      name: "Pencil Box"
    },
    23948234888: {
      quantity: 1,
      price: "$8",
      name: "Geometry Box"
    },
  }]
  
  
  /*
    Q1. use Reduce to calculate totalPrice of all the items 
    [ Total Price = each Item Price * quantity]
  
    Q2. Sort items based on price.
  
    Q3. const money = "$2930.229"
  
        - Split the decimal and integer part of the decimal.
        - Round the number to 2 decimal places
  
    Q4. Write a method to Check and see if an address is a valid IP Address?
      examples =    "0.0.0.0" ,  "343.2.1.4"
  
    Q5. write a single method to convert the following 
       { first: "max", middle: "zeT", last: "Payne" } to "Max Zet Payne"
       { first: "henry", "last": "arnolD"} to "Henry Arnold"
  
    Q6. Write a single method that takes 3 params
        (domain - string, security - boolean, port - number)
        port - default value 80
        security - default value 1
  and do conversion
        (domain - "google.com", security - 1)
         output:      `https://google.com:80`
        
        (domain - "man-utd", security - 0, port - 3000)
        output:       `http://manutd.com:3000`
  
  
  */
  data.forEach(element => {
 const array=Object.values(element)
 console.log(array)
  })
// let a= array.reduce((prev,curr) => {
//  const   totalprice = prev + (curr.quantity*number(curr.price.split(($)[1])*78));
// return totalprice
//  })
// // console.log(a)