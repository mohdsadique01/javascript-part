/*Note: For all file operations use fs.promises

Q1. Create 2 files simultaneously.
Wait for 2 seconds and starts deleting them one after another.
(Finish deleting all the files no matter what)


Q2. Create a new file. 
Do File Read and write data to another file
Delete the original file 

    A. using fs.promises chaining
    B. using await keyword


Q3.  
function login(user, val) {
    if(val % 2 === 0) {
        return Promise.resolve(user);
    }
    return Promise.reject(null);
}

function getData() {
    return Promise.resolve("Data");
}

function logData(user, activity) {
    // use fs.promises to save activity in some file
}

Use appropriate method to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    Hint: [Chain/call and await] logData with login function and getData function
    */
//    const fs = require('fs')
//     const fsPromises = require('fs').promises
   
//    async function createfile(){
//     const  data1 =("created file")
//     const data2 =("created")
//    fsPromises.writeFile("./createfile1.js", data1,(error) => {})
//    fsPromises.writeFile("./createfile.js",data2,(error) => {})

//    setTimeout(()=>{

//     fsPromises.unlink("createfile1.js")
//     fsPromises.unlink("createfile.js")
    

// },2000)
//    }
//    createfile()
//    .then(res=>{
//     console.log("created successfully");
// })


// fsPromises.writeFile("./createfile1.js", data1,(error) => {})
// fsPromises.writeFile("./createfile.js",data2,(error) => {})

// setTimeout(()=>{

//  fsPromises.unlink("createfile1.js")
//  fsPromises.unlink("createfile.js")
 

// },2000)
const fs=require('fs')
const fsPromise=require('fs').promises
async function re(){
    const data1=("my name is anuj")
    fsPromise.writeFile("./dubbed.js",data1,(err)=>{})
    fsPromise.readFile("./dubbed.js")
    //fsPromise.unlink("./promise/dubbed.js")

}

re().then((res)=>console.log("successful"))