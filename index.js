const csv = require('csv-parser')
const fs = require('fs')
const path = require('path')
const matches = [];
const deliveries = [];
fs.createReadStream('data/matches.csv')
  .pipe(csv())
  .on('data', (data) => matches.push(data))
  .on('end', () => {
   // console.log(results);
   matchesPlayed(matches); 
   matcheswinbyteam(matches)
  //  ecnomicalbowler(matches)
  });
 
fs.createReadStream('data/deliveries.csv')
  .pipe(csv())
  .on('data', (data) => deliveries.push(data))
  .on('end', () => {
   // console.log(results);
   extraDeliveriespermatch(deliveries,matches)

   ecnomicalBowler(deliveries,matches)
 //  console.log(deliveries)
  });
 


const matchesPlayed = (matches) => {
    const matchesPlayed = {};
  // console.log(matches)
    matches.forEach(element => {
    if(matchesPlayed[element.season]){
            matchesPlayed[element.season]++;
        }
        else {
            matchesPlayed[element.season]=1;
        }
        
    });
     console.log(matchesPlayed);
     const pathFile = path.join(__dirname,"../ipl-project/output/matchesPlayed.json")
     fs.writeFileSync(pathFile, JSON.stringify(matchesPlayed), {encoding: 'utf8'});
   // return matchesplayed;
}
// console.log(matchesPlayed);

 // console.log(matchesPlayed);


 const matcheswinbyteam = (matches) => {
  let result = {};
      for (let i = 0; i < matches.length; i++) {
        const match = matches[i];
        if (!result[match.season]) {
            result[match.season] = {}
        }
        if (!result[match.season][match.winner]) {
            result[match.season][match.winner] = 1
        } else {
            result[match.season][match.winner] = result[match.season][match.winner] + 1
        }
    
      }
     // console.log(result)
    const pathFile = path.join(__dirname,"../ipl-project/output/matcheswinbyteam.json")
    fs.writeFileSync(pathFile, JSON.stringify(result), {encoding: 'utf8'});

}
const  extraDeliveriespermatch = (deliveries,matches) =>{

  let  seasonIds = matches.filter((element) => element.season == 2016 ).map((match)=>match.id);
  let uniqueSeasonsIds = [... new Set(seasonIds)];
  let data =  deliveries.filter((ele)=>uniqueSeasonsIds.includes(ele.match_id));
   let extraRunOfPerTeam = data.reduce((group, team) => {
           group[team.bowling_team] = (group[team.bowling_team] || 0) + Number(team.extra_runs);
           return group;
         }, {})
  console.log(extraRunOfPerTeam)
  
     const pathFile = path.join(__dirname,"../ipl-project/output/extraDeliveriespermatch.json")
      fs.writeFileSync(pathFile, JSON.stringify(extraRunOfPerTeam), {encoding: 'utf8'});
      
 }

const ecnomicalBowler = (deliveries,matches) =>  {
  let runRateOfBowler = matches.reduce((group, team) => {
    group[team.bowler] = (group[team.bowler] || 0) + ( Number(team.total_runs) / Number(team.over));
    return group;
  }, {});
  let  bowlers = [];
  let keys = Object.keys(runRateOfBowler);
  for (let i=0; i< keys.length; i++){
    let key = keys[i]
    let obj =  {
      [key]: (runRateOfBowler[key]).toFixed(2)
    }
    bowlers.push(obj);
  }
  const topTenBowler = bowlers.sort((a,b) =>{
    let ap=Object.values(a)[0];
    let bp=Object.values(b)[0];
    return ap-bp; }
 
  ).splice(0,10)    
  console.log(topTenBowler,'sorted team');
  //  const pathFile = path.join(__dirname,"../ipl-project/output/eomicalBowler.json")
    // fs.writeFileSync(pathFile, JSON.stringify(topTenBowler), {encoding: 'utf8'});

}